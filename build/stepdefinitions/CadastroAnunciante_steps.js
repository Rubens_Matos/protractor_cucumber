"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Painel_pages_1 = require("../pages/Painel_pages");
const chai = require("chai");
const Anunciante_pages_1 = require("../pages/Anunciante_pages");
const faker = require("faker");
const attach_1 = require("../support/attach");
chai.use(require('chai-smoothie'));
const expect = chai.expect;
const should = chai.should();
const assert = chai.assert;
const { Given, When, Then } = require("cucumber");
const painel = new Painel_pages_1.Painel();
const anunciante = new Anunciante_pages_1.Anunciante();
const scr = new attach_1.Screeshot();
let firstName;
let lastName;
let email;
Given('o Painel inicial', () => __awaiter(this, void 0, void 0, function* () {
    let label = yield painel.btnPainel.getText();
    expect(label).to.eq('Painel');
}));
When('clicar na opção {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    yield painel.contas.click();
}));
Then('o sistema redireciona para a tela de Contas', () => __awaiter(this, void 0, void 0, function* () {
    let label = yield anunciante.menuAnunciante.getText();
    expect(label).to.equals('Contas de Anunciantes');
}));
Then('quando clicar para adicionar um novo anunciante', () => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.btnAdicionar.click();
}));
Then('o sistema abre a tela {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    let labelAdicionar = yield anunciante.labelAdicionar.getText();
    expect(labelAdicionar).to.eq(string);
}));
Then('quando eu preencher os campos do cadastro', () => __awaiter(this, void 0, void 0, function* () {
    firstName = faker.name.firstName();
    lastName = faker.name.lastName();
    email = `${firstName}.${lastName}@email.com`;
    yield anunciante.campoEmail.sendKeys(email.toLocaleLowerCase());
    yield anunciante.campoNome.sendKeys(firstName);
    yield anunciante.campoSobrenome.sendKeys(lastName);
}));
Then('clicar em {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.btnSalvar.click();
}));
Then('o sistema emite a mensagem {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    let msg = yield anunciante.msgSucesso.getText();
    expect(msg).to.eq(string);
}));
Given('a tela de consulta de contas de anunciantes', () => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.menuContasAnunciantes.click();
}));
When('digitar o nome do anunciante cadastrado', () => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.campoPesquisa.sendKeys(firstName);
    yield anunciante.campoPesquisa.submit();
}));
When('clicar no botão Buscar', () => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.btnBuscar.click();
}));
Then('o cadastro do anunciante deve ser exibido na listagem', () => __awaiter(this, void 0, void 0, function* () {
    yield anunciante.primeiroRegistro.click();
}));
