"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const LoginAplicacao_page_1 = require("../pages/LoginAplicacao_page");
const chai = require("chai");
chai.use(require('chai-smoothie'));
const expect = chai.expect;
const should = chai.should();
const assert = chai.assert;
const { Given, When, Then } = require("cucumber");
const login = new LoginAplicacao_page_1.LoginAPlicacao();
Given('o endereço da apĺicação', () => __awaiter(this, void 0, void 0, function* () {
    //abstraído pelo config.ts    
}));
When('clicar no botão {string}', (string) => __awaiter(this, void 0, void 0, function* () {
    yield login.btnEntre.click();
}));
Then('o sistema deve apresentar o Painel', () => __awaiter(this, void 0, void 0, function* () {
    let texto = yield login.labelPainel.getText();
    texto.should.contains('ATIVIDADES DA LINHA DO TEMPO');
    expect(texto).to.contains('ATIVIDADES DA LINHA DO TEMPO');
    assert.exists(texto, 'ATIVIDADES DA LINHA DO TEMPO');
}));
