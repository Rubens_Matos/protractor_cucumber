"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
class Screeshot {
    // abstract writing screen shot to a file
    writeScreenShot(data, filename) {
        var stream = fs.createWriteStream(filename);
        stream.write(new Buffer(data, 'base64'));
        stream.end();
    }
}
exports.Screeshot = Screeshot;
