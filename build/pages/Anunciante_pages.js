"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class Anunciante {
    constructor() {
        this.btnAdicionar = protractor_1.element(protractor_1.by.css('#view-content-list > div.content-control > div > div.col-md-3.col-sm-12.control-add > div > a'));
        this.campoEmail = protractor_1.element(protractor_1.by.id('username'));
        this.campoNome = protractor_1.element(protractor_1.by.id('first_name'));
        this.campoSobrenome = protractor_1.element(protractor_1.by.id('last_name'));
        this.btnSalvar = protractor_1.element(protractor_1.by.css('body > main > form > section.row.footer-action > div > div > div > button'));
        this.menuAnunciante = protractor_1.element(protractor_1.by.css('#sidebar > div > ul > li.active > a'));
        this.labelAdicionar = protractor_1.element(protractor_1.by.css('body > main > form > section.row.heading > div > div > h1'));
        this.msgSucesso = protractor_1.element(protractor_1.by.css('body > main > form > div > h4'));
        this.menuContasAnunciantes = protractor_1.element(protractor_1.by.css('#sidebar > div > ul > li.active > a'));
        this.campoPesquisa = protractor_1.element(protractor_1.by.name('search_username'));
        this.btnBuscar = protractor_1.element(protractor_1.by.css('#view-content-list > div.content-control > div > div.col-md-4.col-sm-8.col-xs-6.control-search > div > form > div > div > div > button > span'));
        this.primeiroRegistro = protractor_1.element(protractor_1.by.xpath('//*[@id="view-content-list"]/div[2]/div[1]/section/ul/li[1]/div[2]'));
    }
}
exports.Anunciante = Anunciante;
