"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class LoginAPlicacao {
    constructor() {
        this.btnEntre = protractor_1.element(protractor_1.by.buttonText('Entre'));
        this.labelPainel = protractor_1.element(protractor_1.by.css('body > main > div > section.timeline > div.timeline-control > div.tml-heading > h2'));
    }
}
exports.LoginAPlicacao = LoginAPlicacao;
