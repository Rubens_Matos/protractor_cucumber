"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class Painel {
    constructor() {
        this.navUser = protractor_1.element(protractor_1.by.css('#navUser > span > i'));
        this.contas = protractor_1.element(protractor_1.by.css('body > main > div > section.row.panels > div:nth-child(4) > a'));
        this.btnPainel = protractor_1.element(protractor_1.by.id('navDashboard'));
    }
}
exports.Painel = Painel;
